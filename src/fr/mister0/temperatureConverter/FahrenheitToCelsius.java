package fr.mister0.temperatureConverter;

public class FahrenheitToCelsius {

	public static double calcul(double nb) {
		double temp = (nb - 32) * 5 / 9;
		return temp;
	}

}
