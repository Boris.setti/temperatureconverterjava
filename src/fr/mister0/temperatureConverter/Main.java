package fr.mister0.temperatureConverter;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		double temp;
		char choix, continu;
		do {

			do {
				System.out.println("Choisissez le mode de conversion :");
				System.out.println("1 - Convertisseur Celsius - Fahrenheit");
				System.out.println("2 - Convertisseur Fahrenheit  - Celsius");

				choix = sc.nextLine().charAt(0);

				if (choix != '1' && choix != '2') {
					System.out.println("Mode inconnu, veuillez réitérer votre choix.");

				}
			} while (choix != '1' && choix != '2');

			if (choix == '1') {
				System.out.println("Temperature à convertir :");
				temp = sc.nextDouble();
				System.out.println(temp + "°C = " + CelsiusToFahrenhheit.calcul(temp) + "°F");
			} else if (choix == '2') {
				System.out.println("Temperature à convertir :");
				temp = sc.nextDouble();
				System.out.println(temp + "°F = " + FahrenheitToCelsius.calcul(temp) + "°C");
			}
			sc.nextLine();
			do {
				System.out.println("Souhaitez-vous convertir une autre température ?(O/N)");
				continu = sc.nextLine().charAt(0);

				if (continu != 'O' && continu != 'N') {
					System.out.println("Commande inconnu, veuillez réitérer votre choix.");
				}
			} while (continu != 'O' && continu != 'N');

		} while (continu == 'O');
		System.out.println("Au revoir !");
		sc.close();
	}

}
